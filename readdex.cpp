// readdex.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "readdex.h"
int dvmDexFileOpenFromFd(int fd, DvmDex** ppDvmDex);
static DvmDex* allocateAuxStructures(DexFile* pDexFile);
DexFile* dexFileParse(const u1* data, size_t length, int flags);
int sysMapFileInShmemWritableReadOnly(int fd, MemMapping* pMap);
void sysReleaseShmem(MemMapping* pMap);
void dexFileFree(DexFile* pDexFile);
void sysCopyMap(MemMapping* dst, const MemMapping* src);
int sysFakeMapFile(int fd, MemMapping* pMap);
static int getFileStartAndLength(int fd, off_t *start_, size_t *length_);
void dexFileSetupBasicPointers(DexFile* pDexFile, const u1* data);
bool dexHasValidMagic(const DexHeader* pHeader);
bool initialstringlist(DvmDex* );



void myStringObj::printString()
{
	for (u4 i=0; i<numbyte; i++)
		printf("%c",*(str+i));
	printf(" ");
}
int _tmain(int argc, _TCHAR* argv[])
{
	int fd= _open("D:\\classes.dex",O_RDONLY);
	DvmDex *pDvmDex;
	dvmDexFileOpenFromFd(fd,&pDvmDex);


	
	getchar();

}

int dvmDexFileOpenFromFd(int fd, DvmDex** ppDvmDex)
{
    DvmDex* pDvmDex;
    DexFile* pDexFile;
    MemMapping memMap;
    int parseFlags = kDexParseDefault;
    int result = -1;
	/*
    if (gDvm.verifyDexChecksum)
        parseFlags |= kDexParseVerifyChecksum;
*/
	/*
    if (_lseek(fd, 0, SEEK_SET) < 0) {
        printf("lseek rewind failed");
        goto bail;
    }*/
	
    if (sysMapFileInShmemWritableReadOnly(fd, &memMap) != 0) {
        printf("Unable to map file");
        goto bail;
    }

    pDexFile = dexFileParse((u1*)memMap.addr, memMap.length, parseFlags);
    if (pDexFile == NULL) {
        printf("DEX parse failed");
        sysReleaseShmem(&memMap);
        goto bail;
    }

    pDvmDex = allocateAuxStructures(pDexFile);
    if (pDvmDex == NULL) {
        dexFileFree(pDexFile);
        sysReleaseShmem(&memMap);
        goto bail;
    }

	

    /* tuck this into the DexFile so it gets released later */
    sysCopyMap(&pDvmDex->memMap, &memMap);
	initialstringlist(pDvmDex);
    pDvmDex->isMappedReadOnly = true;
    *ppDvmDex = pDvmDex;
    result = 0;

bail:
    return result;
}

static DvmDex* allocateAuxStructures(DexFile* pDexFile)
{
    DvmDex* pDvmDex;
    const DexHeader* pHeader;
    u4 stringCount, classCount, methodCount, fieldCount;

    pDvmDex = (DvmDex*) calloc(1, sizeof(DvmDex));
    if (pDvmDex == NULL)
        return NULL;

    pDvmDex->pDexFile = pDexFile;
    pDvmDex->pHeader = pDexFile->pHeader;

    pHeader = pDvmDex->pHeader;

    stringCount = pHeader->stringIdsSize;
    classCount = pHeader->typeIdsSize;
    methodCount = pHeader->methodIdsSize;
    fieldCount = pHeader->fieldIdsSize;

    pDvmDex->pResStrings = (myStringObj**)calloc(stringCount, sizeof(u4*));
/*
    pDvmDex->pResClasses = (struct ClassObject**)
        calloc(classCount, sizeof(struct ClassObject*));

    pDvmDex->pResMethods = (struct Method**)
        calloc(methodCount, sizeof(struct Method*));

    pDvmDex->pResFields = (struct Field**)
        calloc(fieldCount, sizeof(struct Field*));
*/
    printf("+++ DEX %p: allocateAux %d+%d+%d+%d * 4 = %d bytes",
        pDvmDex, stringCount, classCount, methodCount, fieldCount,
        (stringCount + classCount + methodCount + fieldCount) * 4);

    //pDvmDex->pInterfaceCache = dvmAllocAtomicCache(DEX_INTERFACE_CACHE_SIZE);

    if (pDvmDex->pResStrings == NULL /*||
        pDvmDex->pResClasses == NULL ||
        pDvmDex->pResMethods == NULL ||
        pDvmDex->pResFields == NULL ||
        pDvmDex->pInterfaceCache == NULL*/)
    {
        printf("Alloc failure in allocateAuxStructures");
        free(pDvmDex->pResStrings);
   /*     free(pDvmDex->pResClasses);
        free(pDvmDex->pResMethods);
        free(pDvmDex->pResFields);
        free(pDvmDex);*/
        return NULL;
    }

    return pDvmDex;

}
int sysMapFileInShmemWritableReadOnly(int fd, MemMapping* pMap)
{
    return sysFakeMapFile(fd, pMap);

}
int sysFakeMapFile(int fd, MemMapping* pMap)
{
    /* No MMAP, just fake it by copying the bits.
       For Win32 we could use MapViewOfFile if really necessary
       (see libs/utils/FileMap.cpp).
    */
    off_t start;
    size_t length;
    void* memPtr;

    assert(pMap != NULL);

    if (getFileStartAndLength(fd, &start, &length) < 0)
        return -1;

    memPtr = malloc(length);
    if (_read(fd, memPtr, length) < 0) {
        printf("read(fd=%d, start=%d, length=%d) failed: %s", (int) length,
            fd, (int) start, strerror(errno));
        return -1;
    }

    pMap->baseAddr = pMap->addr = memPtr;
    pMap->baseLength = pMap->length = length;

    return 0;
}
static int getFileStartAndLength(int fd, off_t *start_, size_t *length_)
{
    off_t start, end;
    size_t length;

    assert(start_ != NULL);
    assert(length_ != NULL);

    start = _lseek(fd, 0L, SEEK_CUR);
    end = _lseek(fd, 0L, SEEK_END);
    (void) _lseek(fd, start, SEEK_SET);

    if (start == (off_t) -1 || end == (off_t) -1) {
        printf("could not determine length of file");
        return -1;
    }

    length = end - start;
    if (length == 0) {
        printf("file is empty");
        return -1;
    }

    *start_ = start;
    *length_ = length;

    return 0;
}

DexFile* dexFileParse(const u1* data, size_t length, int flags)
{
    DexFile* pDexFile = NULL;
    const DexHeader* pHeader;
    const u1* magic;
    int result = -1;

    if (length < sizeof(DexHeader)) {
        printf("too short to be a valid .dex");
        goto bail;      /* bad file format */
    }

    pDexFile = (DexFile*) malloc(sizeof(DexFile));
    if (pDexFile == NULL)
        goto bail;      /* alloc failure */
    memset(pDexFile, 0, sizeof(DexFile));

    /*
     * Peel off the optimized header.
     */
    if (memcmp(data, DEX_OPT_MAGIC, 4) == 0) {
        magic = data;
        if (memcmp(magic+4, DEX_OPT_MAGIC_VERS, 4) != 0) {
            printf("bad opt version (0x%02x %02x %02x %02x)",
                 magic[4], magic[5], magic[6], magic[7]);
            goto bail;
        }

        pDexFile->pOptHeader = (const DexOptHeader*) data;
        printf("Good opt header, DEX offset is %d, flags=0x%02x",
            pDexFile->pOptHeader->dexOffset, pDexFile->pOptHeader->flags);

        /* parse the optimized dex file tables */
       // if (!dexParseOptData(data, length, pDexFile))
          //  goto bail;

        /* ignore the opt header and appended data from here on out */
        data += pDexFile->pOptHeader->dexOffset;
        length -= pDexFile->pOptHeader->dexOffset;
        if (pDexFile->pOptHeader->dexLength > length) {
            printf("File truncated? stored len=%d, rem len=%d",
                pDexFile->pOptHeader->dexLength, (int) length);
            goto bail;
        }
        length = pDexFile->pOptHeader->dexLength;
    }

    dexFileSetupBasicPointers(pDexFile, data);
    pHeader = pDexFile->pHeader;

    if (!dexHasValidMagic(pHeader)) {
        goto bail;
    }

    /*
     * Verify the checksum(s).  This is reasonably quick, but does require
     * touching every byte in the DEX file.  The base checksum changes after
     * byte-swapping and DEX optimization.
     */
	/*

    if (flags & kDexParseVerifyChecksum) {
        u4 adler = dexComputeChecksum(pHeader);
        if (adler != pHeader->checksum) {
            printf("ERROR: bad checksum (%08x vs %08x)",
                adler, pHeader->checksum);
            if (!(flags & kDexParseContinueOnError))
                goto bail;
        } else {
            printf("+++ adler32 checksum (%08x) verified", adler);
        }

        const DexOptHeader* pOptHeader = pDexFile->pOptHeader;
        if (pOptHeader != NULL) {
            adler = dexComputeOptChecksum(pOptHeader);
            if (adler != pOptHeader->checksum) {
                printf("ERROR: bad opt checksum (%08x vs %08x)",
                    adler, pOptHeader->checksum);
                if (!(flags & kDexParseContinueOnError))
                    goto bail;
            } else {
                printf("+++ adler32 opt checksum (%08x) verified", adler);
            }
        }
    }*/

    /*
     * Verify the SHA-1 digest.  (Normally we don't want to do this --
     * the digest is used to uniquely identify the original DEX file, and
     * can't be computed for verification after the DEX is byte-swapped
     * and optimized.)
     */
	/*
    if (kVerifySignature) {
        unsigned char sha1Digest[kSHA1DigestLen];
        const int nonSum = sizeof(pHeader->magic) + sizeof(pHeader->checksum) +
                            kSHA1DigestLen;

        dexComputeSHA1Digest(data + nonSum, length - nonSum, sha1Digest);
        if (memcmp(sha1Digest, pHeader->signature, kSHA1DigestLen) != 0) {
            char tmpBuf1[kSHA1DigestOutputLen];
            char tmpBuf2[kSHA1DigestOutputLen];
            printf("ERROR: bad SHA1 digest (%s vs %s)",
                dexSHA1DigestToStr(sha1Digest, tmpBuf1),
                dexSHA1DigestToStr(pHeader->signature, tmpBuf2));
            if (!(flags & kDexParseContinueOnError))
                goto bail;
        } else {
            printf("+++ sha1 digest verified");
        }
    }

    if (pHeader->fileSize != length) {
        printf("ERROR: stored file size (%d) != expected (%d)",
            (int) pHeader->fileSize, (int) length);
        if (!(flags & kDexParseContinueOnError))
            goto bail;
    }

    if (pHeader->classDefsSize == 0) {
        printf("ERROR: DEX file has no classes in it, failing");
        goto bail;
    }
	*/

    /*
     * Success!
     */
    result = 0;

bail:
    if (result != 0 && pDexFile != NULL) {
        dexFileFree(pDexFile);
        pDexFile = NULL;
    }
    return pDexFile;
}

void sysReleaseShmem(MemMapping* pMap)
{
    /* Free the bits allocated by sysMapFileInShmem. */
    if (pMap->baseAddr != NULL) {
      free(pMap->baseAddr);
      pMap->baseAddr = NULL;
    }
    pMap->baseLength = 0;

}

void dexFileFree(DexFile* pDexFile)
{
    if (pDexFile == NULL)
        return;

    free(pDexFile);
}

void sysCopyMap(MemMapping* dst, const MemMapping* src)
{
    memcpy(dst, src, sizeof(MemMapping));
}

void dexFileSetupBasicPointers(DexFile* pDexFile, const u1* data) {
    DexHeader *pHeader = (DexHeader*) data;

    pDexFile->baseAddr = data;
    pDexFile->pHeader = pHeader;
    pDexFile->pStringIds = (const DexStringId*) (data + pHeader->stringIdsOff);
	/*
	printf("%x\n",pDexFile->pStringIds[0]); 
	
	u4 a=pDexFile->pStringIds[1].stringDataOff;
	int size=*(data+a);
	printf("%x\n",size);
	int ee=size*sizeof(u1);
	printf("%d",ee);
	void* aaa=malloc(ee);

	//memcpy(&aaa,data+a+1,ee);
	//printf("%x",aaa);
	free(aaa);
	*/


    //pDexFile->pTypeIds = (const DexTypeId*) (data + pHeader->typeIdsOff);
   // pDexFile->pFieldIds = (const DexFieldId*) (data + pHeader->fieldIdsOff);
    //pDexFile->pMethodIds = (const DexMethodId*) (data + pHeader->methodIdsOff);
    //pDexFile->pProtoIds = (const DexProtoId*) (data + pHeader->protoIdsOff);
    //pDexFile->pClassDefs = (const DexClassDef*) (data + pHeader->classDefsOff);
    //pDexFile->pLinkData = (const DexLink*) (data + pHeader->linkOff);
}

bool dexHasValidMagic(const DexHeader* pHeader)
{
    const u1* magic = pHeader->magic;
    const u1* version = &magic[4];

    if (memcmp(magic, DEX_MAGIC, 4) != 0) {
        printf("ERROR: unrecognized magic number (%02x %02x %02x %02x)",
            magic[0], magic[1], magic[2], magic[3]);
        return false;
    }

    if ((memcmp(version, DEX_MAGIC_VERS, 4) != 0) &&
            (memcmp(version, DEX_MAGIC_VERS_API_13, 4) != 0)) {
        /*
         * Magic was correct, but this is an unsupported older or
         * newer format variant.
         */
        printf("ERROR: unsupported dex version (%02x %02x %02x %02x)",
            version[0], version[1], version[2], version[3]);
        return false;
    }

    return true;
}
static bool isValidPointer(const void* ptr, const void* start, const void* end)
{
    return (ptr >= start) && (ptr < end) && (((u4) ptr & 7) == 0);
}


bool initialstringlist(DvmDex* pDvmDex)
{
	u4 stridssize=pDvmDex->pDexFile->pHeader->stringIdsSize;
	//printf ("\n%d\n",stridssize);
	//printf ("%x\n",pDvmDex->memMap.addr);
	//printf ("%x\n",pDvmDex->pDexFile->baseAddr);
	
	
	for(u4 i=0;i<stridssize;i++)
	{
		u4 strdataoff=pDvmDex->pDexFile->pStringIds[i].stringDataOff;
		//printf ("%x\n",strdataoff);
		const u1 *strdataaddr=pDvmDex->pDexFile->baseAddr+strdataoff;
		myStringObj* tmpstrobj=(myStringObj*)malloc(sizeof(myStringObj*));
		tmpstrobj->numbyte=*(strdataaddr);
		printf("No %d string, number of the string's byte: %d\n",i,tmpstrobj->numbyte);
		tmpstrobj->str=strdataaddr+1;
        pDvmDex->pResStrings[i]=tmpstrobj;
		pDvmDex->pResStrings[i]->printString();
		printf("\n");
		

	}
	
	return true;
}
